package it.unibo.oop.lab.advanced;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {

    private final int min;
    private final int max;
    private final int attempts;
    private final DrawNumber model;
    private final DrawNumberView view;

    /**
     * @throws IOException 
     * 
     */
    public DrawNumberApp() {
        List<Integer> configList = new ArrayList<Integer>();
        try (
            var contents = new BufferedReader(
                    new InputStreamReader(ClassLoader.getSystemResourceAsStream("config.yml")))
        ) {
            for (var configLine = contents.readLine(); configLine != null; configLine = contents.readLine()) {
                StringTokenizer st = new StringTokenizer(configLine);
                while (st.hasMoreTokens()) {
                    st.nextToken();
                }
                configList.add(Integer.parseInt(st.toString()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.min = configList.get(0);
        this.max = configList.get(1);
        this.attempts = configList.get(2);
        this.model = new DrawNumberImpl(min, max, attempts);
        this.view = new DrawNumberViewImpl();
        this.view.setObserver(this);
        this.view.start();
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            this.view.result(result);
        } catch (IllegalArgumentException e) {
            this.view.numberIncorrect();
        } catch (AttemptsLimitReachedException e) {
            view.limitsReached();
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        System.exit(0);
    }

    /**
     * @param args
     *                 ignored
     */
    public static void main(final String... args) {
        new DrawNumberApp();
    }

}
