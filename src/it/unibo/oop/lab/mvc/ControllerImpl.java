package it.unibo.oop.lab.mvc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A controller that prints strings and has memory of the strings it printed.
 */
public class ControllerImpl implements Controller {

    private String nextString;
    private List<String> stringHistory;

    public ControllerImpl() {
        this.nextString = new String();
        this.stringHistory = new ArrayList<String>();
    }

    /**
     * A method for setting the next string to print.
     * @param str
     *      String The string that have to set
     * @throws IOException If str == null
     */
    public void setNextStringToPrint(final String str) throws IOException {
        if (str == null) {
            throw new IOException();
        }
        this.nextString = str;
    }

    /**
     * A method for getting the next string to print.
     * @return the string
     */
    public String getNextStringToPrint() {
        return this.nextString;
    }

    /**
     * A method for getting the history of the printed strings (in form of a
     * List of Strings).
     * @return the List with all the strings
     */
    public List<String> getStringHistory() {
        return this.stringHistory;
    }

    /**
     * A method that prints the current string. If the current string is
     * unset, an IllegalStateException should be thrown.
     * @throws IllegalStateException if the string is null
     */
    public void printCurrentString() throws IllegalStateException{
        if (this.nextString == null) {
            throw new IllegalStateException();
        }
        System.out.println(this.nextString);
        this.stringHistory.add(this.nextString);
    }

}
